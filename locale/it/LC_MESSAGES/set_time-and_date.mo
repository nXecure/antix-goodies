��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    6   �       $     #   ,  %   P     v     {     �     �  =   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-25 22:19+0300
Last-Translator: Pierluigi Mario <pierluigimariomail@gmail.com>, 2021
Language-Team: Italian (https://www.transifex.com/antix-linux-community-contributions/teams/120110/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.3
 Scegli il fuso orario (usando i tasti cursore e Invio) Data: Configurazione di Imposta Data e Ora Sposta il cursore sull'ora corretta Sposta il cursore sul Minuto corretto Esci Seleziona il fuso orario Imposta la data corrente Imposta l'ora corrente Utilizzare l'ora del server Internet per impostare data e ora 