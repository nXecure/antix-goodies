��          t      �         .        @     F  #   d  %   �     �     �     �     �  6   �  �    E   �     �  )     )   .  (   X     �     �     �     �  D   �                       	                    
                 Choose Time Zone (using cursor and enter keys) Date: Manage Date and Time Settings Move the slider to the correct Hour Move the slider to the correct Minute Quit Select Time Zone Set Current Date Set Current Time Use Internet Time server to set automaticaly time/date Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-10-25 22:19+0300
Last-Translator: Wallon Wallon, 2021
Language-Team: French (Belgium) (https://www.transifex.com/antix-linux-community-contributions/teams/120110/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.3
 Choisir le fuseau horaire (en utilisant les touches curseur et Enter) Date : Gérer les paramètres de date et d'heure Déplacez le curseur sur l'heure correcte Déplacez le curseur sur la bonne minute Quitter Sélectionnez le fuseau horaire Définir la date actuelle Régler l'heure actuelle Utiliser l'heure du serveur internet pour régler la date et l'heure 