# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Eduard Selma <selma@tinet.cat>, 2021
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-03-01 11:32+0200\n"
"PO-Revision-Date: 2020-02-26 16:23+0000\n"
"Last-Translator: Eduard Selma <selma@tinet.cat>, 2021\n"
"Language-Team: Catalan (https://www.transifex.com/anticapitalista/teams/10162/ca/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ca\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: fast_personal_menu_editor.sh:11 fast_personal_menu_editor.sh:26
msgid "Fast Personal Menu Manager for IceWM"
msgstr "Gestor personal de menú per IceWM"

#: fast_personal_menu_editor.sh:12
msgid "App .desktop file"
msgstr "Fitxer App .desktop"

#: fast_personal_menu_editor.sh:14
msgid "REMOVE last entry"
msgstr "ELIMINA la darrera entrada"

#: fast_personal_menu_editor.sh:15
msgid "ORGANIZE entries"
msgstr "ORGANITZA entrades"

#: fast_personal_menu_editor.sh:16
msgid "UNDO last change"
msgstr "DESFÉS el darrer canvi "

#: fast_personal_menu_editor.sh:17
msgid "ADD selected app"
msgstr "Afegeix l'app seleccionada"

#: fast_personal_menu_editor.sh:18
msgid ""
"Choose (or drag and drop to the field below) the .desktop file you want to "
"add to the personal menu \\n OR select any other option"
msgstr ""
"Trieu (o arrossegueu i deixeu anar) el fitxer .desktop que voleu afegir al "
"vostre menú personal \\n O seleccioneu qualsevol altra opció"

#: fast_personal_menu_editor.sh:26
msgid ""
"FPM has no 'graphical' way to allow users to move icons around or delete "
"arbitrary icons.\\nIf you click OK, the personal menu configuration file "
"will be opened for editing.\\nEach menu icon is identified by a line "
"starting with 'prog' followed by the application name, icon location and the"
" application executable file.\\nMove or delete the entire line refering to "
"each personal menu entry.\\nNote: Lines starting with # are comments only "
"and will be ignored.\\nThere can be empty lines.\\nSave any changes and then"
" restart IceWM.\\nYou can undo the last change from FPMs 'Restore' button."
msgstr ""
"FPM no té un mode \"gràfic\" per permetre als nous usuaris moure icones o "
"esborrar-ne arbitràriament.\\nSi cliqueu OK, s'obrirà la configuració del "
"menú personal per editar-lo.\\nCada icona del menú s'identifica amb una "
"línia que comença amb 'prog', seguida del nom de l'aplicació, situació de la"
" icona i el fitxer executable de l'aplicació.\\nMogueu o esborreu la línia "
"sencera referida a cada entrada del menú personal.\\nNota: Les línies que "
"comencen per # són comentaris i s'ignoraran.\\nPoden haver-hi línies "
"buides.\\nDeseu els canvis i torneu a arrencar IceWM.\\nPodeu desfer el "
"darrer canvi amb el botó \"Restaura\" de FPM."

#: fast_personal_menu_editor.sh:38 fast_personal_menu_editor.sh:42
#: fast_personal_menu_editor.sh:109
msgid "Warning"
msgstr "Atenció "

#: fast_personal_menu_editor.sh:38
msgid "FTM is programmed to always keep 1 line in the personal menu file!"
msgstr ""
"FPM està programat per mantenir sempre 1 línia al fitxer de menú personal!"

#: fast_personal_menu_editor.sh:42
msgid "This will delete the last entry from your personal menu! Are you sure?"
msgstr ""
"Això esborrarà la darrera entrada del vostre menú personal! N'esteu segurs?"

#: fast_personal_menu_editor.sh:109
msgid "No changes were made! Please choose an application."
msgstr "No s'ha fet cap canvi! Si us plau, trieu una aplicació."
