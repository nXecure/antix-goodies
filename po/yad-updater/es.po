# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# German Lancheros <glancheros2015@gmail.com>, 2020
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-02-27 14:09+0200\n"
"PO-Revision-Date: 2020-02-27 12:11+0000\n"
"Last-Translator: German Lancheros <glancheros2015@gmail.com>, 2020\n"
"Language-Team: Spanish (https://www.transifex.com/anticapitalista/teams/10162/es/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: es\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: yad-updater.sh:12
msgid "Internet connection detected"
msgstr "Conexión a Internet detectada"

#: yad-updater.sh:18
msgid "already root"
msgstr "es root"

#: yad-updater.sh:22
msgid "You are Root or running the script in sudo mode"
msgstr "Eres Root o ejecutando el guión en modo sudo"

#: yad-updater.sh:24
msgid "You entered the wrong password or you cancelled"
msgstr "Has introducido una contraseña incorrecta o la has cancelado."

#: yad-updater.sh:31
msgid "Waiting for a Network connection..."
msgstr "Esperando una conexión de red..."

#: yad-updater.sh:42
msgid "No Internet connection detected!"
msgstr "¡No se detecta ninguna conexión a Internet!"
