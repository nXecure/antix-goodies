# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-11-14 22:15+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:31
msgid "Usage: $ME [<options>] [ceni|connman]"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:33
msgid ""
"Switch between ceni and connman easily. Can be launched in terminal\n"
"or gui mode. You can also specify the program directly to reduce steps."
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:36
msgid ""
"\t   ceni:  will automatically switch to ceni to manage your network\n"
"\tconnman:  will remove all ceni configurations and launch connman"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:39
msgid ""
"Options:\n"
"\t-c --cli\tLaunches in terminal mode\n"
"\t-g --gui\tLaunches a yad gui (requires yad to be installed)\n"
"\t-h --help\tShow this usage\n"
"\t-v --version\tShow version information"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:45
msgid ""
"Notes:\n"
"\t- For now only connman and ceni are configured. Specially built for\n"
"\tantiX 19. Should work for any antiX flavor (core, base or full)"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:70
msgid "$SWITCH_NM is not installed. Switching is impossible"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:76
msgid "Option $SWITCH_NM not yet available"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:86
msgid "Select the program to manage your Wi-Fi"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:88
msgid "--field=\\\"CENI!ceni!Switch to ceni\\\":FBTN 'bash -c switch_to_ceni'"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:90
msgid ""
"--field=\\\"CONNMAN!connman!Switch to connman\\\":FBTN 'bash -c "
"switch_to_connman'"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:96
msgid "ceni and connman are not installed. Switching is impossible."
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:102
msgid "Switch impossible"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:111
msgid "echo \\\"connman is not installed. Not a valid option.\\\""
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:115
msgid "echo \\\"ceni is not installed. Not a valid option.\\\""
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:117
msgid "$SELECT_OPTIONS is the only available program."
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:125
msgid "Switch Wi-Fi program"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:137
msgid "Switch to? (${SELECT_MESSAGE} or q to quit) "
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:141
msgid "Exiting $ME without switching."
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:142
msgid "Please answer $SELECT_MESSAGE or q to quit"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:204
msgid "resolv.conf current configuration: $CURRENT_RESOLV_CONF"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:217
msgid "/etc/resolv.conf new symbolic link: $(readlink /etc/resolv.conf)"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:221
msgid ""
"You are using a custom /etc/resolv.conf file. This case is not contemplated "
"by the script."
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:225
msgid "WARNING: you don't have a /etc/resolv.conf file."
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:228
msgid "Your current /etc/resolv.conf file is ideal for $CURRENT_RESOLV_CONF"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:236
msgid "Wi-Fi softblocked."
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:239
msgid "Enabling WIFI with connman"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:243
msgid "starting connman service"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:248
msgid "Restarting connman service just in case"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:252
msgid "Enabling Wi-Fi..."
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:258
msgid "Stopping connman service"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:264
msgid "Unblocking Wi-Fi with rfkill..."
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:274
msgid "WIFI HARD BLOCKED"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:275
msgid ""
"Your Wi-Fi may be Hard blocked. If you cannot scan for any network, unlock "
"the wifi with the corresponding button or BIOS option."
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:289
msgid "Switching to ceni"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:295
msgid "Stopping Connman program and service"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:301
msgid "Removing $CONNMAN_PROGRAM from startup"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:307
#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:374
msgid "Stopping all wpa_supplicant processes"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:309
#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:378
msgid "Restarting networking service"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:311
msgid "Launching ceni"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:315
msgid ""
"<b>ceni</b> should now be able to connect to your Wireless Access Point."
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:317
msgid "CENI set"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:326
msgid "Switching to Connman"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:337
msgid ""
"WLAN entries have been found in /etc/network/interfaces.\\nKeeping them will "
"block connman from properly connecting to Wi-Fi.\\n"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:343
msgid ""
"No WLAN entries were found in /etc/network/interfaces.\\nWe recommend only "
"modifying this file if you keep experiencing \\nwifi connection problems "
"with connman.\\n"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:349
msgid "${CENI_WLAN_CONT}Edit the file?"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:350
msgid "Edit /etc/network/interfaces"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:358
msgid "[Yy]*"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:359
msgid "[Nn]*"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:362
msgid "Modify the file? (y/n) "
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:364
msgid "Deleting info from /etc/network/interfaces"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:366
msgid "/etc/network/interfaces will not be changed"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:367
msgid "Please answer yes or no."
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:376
msgid "Restoring sysvinit connman service"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:380
msgid "Restarting connman service"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:386
msgid "Adding $CONNMAN_PROGRAM to startup"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:389
msgid "Launching $CONNMAN_PROGRAM"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:399
msgid ""
"<b>Connman</b> should now be able to connect to your Wireless Access Point."
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:401
msgid "Connman set"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:404
msgid "Launching connmanctl"
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:476
msgid "yad is not installed or cannot be found."
msgstr ""

#: /home/pc/gitz/antix-wifi-switch/antix-wifi-switch:477
msgid "Defaulting to Terminal"
msgstr ""
